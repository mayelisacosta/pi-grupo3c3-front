import './styless/App.css'
import { Route, Routes } from 'react-router-dom'
import Detail from "./routes/Detail"
import Admin from "./routes/Admin"
import Home from './routes/Home'
import Header from './components/Header'
import ShowCars from './routes/ShowCars'
import Register from './routes/Register'
import InsertVehicle from './routes/InsertVehicle'
import UsersList from './routes/UsersList'
import Footer from './components/Footer'
import Login from './routes/Login'
import Feature from './routes/Feature'
import VehiclesList from './routes/VehiclesList'

function App() {

  
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/detail/:id" element={<Detail />} />
        <Route path="/admin" element={<Admin />} />
        <Route path="/products" element={<ShowCars />} />
        <Route path='/register' element={<Register />} />
        <Route path='/admin/users' element={<UsersList />} />
        <Route path='/admin/insert-vehicle' element={<InsertVehicle />} />
        <Route path="/login" element={<Login />} />
        <Route path='/feature' element={<Feature />} />
        <Route path='/admin/vehicles' element={<VehiclesList />} />
      </Routes>
      <Footer/>
    </>
  )
}

export default App
